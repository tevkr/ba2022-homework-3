package tevkr.com.gitlab

class GameOfThrones(targaryen: Targaryen, lannisters: Lannisters):
  var step: Int = 0
  def nextTurn(targaryenSpell:() => Wealth)(lannistersSpell:() => Wealth): GameOfThrones =
    targaryen.wealth = targaryenSpell()
    lannisters.wealth = lannistersSpell()
    step += 1
    this