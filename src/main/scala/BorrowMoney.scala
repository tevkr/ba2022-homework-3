package tevkr.com.gitlab

trait BorrowMoney:
  this: GreatHouse =>
  def borrowMoney(): Wealth = new Wealth {
    override val amountOfMoney: BigDecimal = wealth.amountOfMoney + 1337
    override val strengthOfArmy: Int = wealth.strengthOfArmy
  }