package tevkr.com.gitlab

case class Targaryen(override val name: String, var wealth: Wealth) extends GreatHouse with MakeWildFire with BorrowMoney