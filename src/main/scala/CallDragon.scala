package tevkr.com.gitlab

trait CallDragon:
  this: GreatHouse =>
  def callDragon(): Wealth = new Wealth {
    override val amountOfMoney: BigDecimal = wealth.amountOfMoney
    override val strengthOfArmy: Int = wealth.strengthOfArmy * 2
  }