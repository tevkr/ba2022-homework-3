package tevkr.com.gitlab

object Main extends App:
  val targaryen = new Targaryen("Targaryen", new Wealth {
    override val amountOfMoney: BigDecimal = 100
    override val strengthOfArmy: Int = 100
  })
  val lannisters = new Lannisters("Lannisters", new Wealth {
    override val amountOfMoney: BigDecimal = 100
    override val strengthOfArmy: Int = 100
  })
  var gameOfThrones = new GameOfThrones(targaryen, lannisters)
  def printState(): Unit =
    println("======= Step: " + gameOfThrones.step + " =======")
    println("States:")
    println(targaryen)
    println(lannisters)
  printState()
  gameOfThrones = gameOfThrones.nextTurn(targaryen.borrowMoney)(lannisters.borrowMoney)
  printState()
  gameOfThrones = gameOfThrones.nextTurn(targaryen.makeWildFire)(lannisters.callDragon)
  printState()