package tevkr.com.gitlab

trait MakeWildFire:
  this: GreatHouse =>
  def makeWildFire(): Wealth = new Wealth {
    override val amountOfMoney: BigDecimal = wealth.amountOfMoney
    override val strengthOfArmy: Int = wealth.strengthOfArmy + 1337
  }