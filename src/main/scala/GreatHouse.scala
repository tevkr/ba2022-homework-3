package tevkr.com.gitlab

trait GreatHouse:
  val name: String
  var wealth: Wealth
  override def toString() : String =
    "{ " + name + ": amountOfMoney=" + wealth.amountOfMoney +
      ", strengthOfArmy=" + wealth.strengthOfArmy + " }";